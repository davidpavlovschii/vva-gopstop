from django.conf.urls import include, url, handler403, handler404, handler500
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

admin.site.site_header = "GopStop page administrateur"
admin.site.index_title = "Bienvenue sur la partie admin de GopStop"

urlpatterns = [
    url(r'^', include('forum.urls', namespace="forum")),
    url(r'^admin/', admin.site.urls)
] 

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler403 = 'gopstop.error.error_403'
handler404 = 'gopstop.error.error_404'
handler500 = 'gopstop.error.error_500'

