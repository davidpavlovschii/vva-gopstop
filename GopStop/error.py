from django.views.defaults import page_not_found, permission_denied, server_error


def error_403(request, exception):
    """
    Return to the 403 page, forbidden access, according the app we are currently in
    """
    if request.path.startswith('/forum/'):
        template_name = 'forum/403.html'
    else:
        template_name = 'landingpage/403.html'
    return permission_denied(request, exception, template_name=template_name)
    # Returns 403 error page of the chosen app


def error_404(request, exception):
    """
    Return to the 404 page, page not found, according the app we are currently in
    """
    if request.path.startswith('/forum/'):
        template_name = 'forum/403.html'
    else:
        template_name = 'landingpage/403.html'
    return page_not_found(request, exception, template_name=template_name)
    # Returns 404 error page of the chosen app


def error_500(request):
    """
    Return to the 500 page, internal server error,  according the app we are currently in
    """
    if request.path.startswith('/forum/'):
        template_name = 'forum/403.html'
    else:
        template_name = 'landingpage/403.html'
    return server_error(request, template_name=template_name)
    # Returns 500 error page of the chosen app
