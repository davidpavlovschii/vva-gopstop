# Generated by Django 2.0.10 on 2019-04-29 18:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0009_auto_20190429_2004'),
    ]

    operations = [
        migrations.RenameField(
            model_name='specialuser',
            old_name='field',
            new_name='site_internet',
        ),
    ]
