# Generated by Django 2.0.10 on 2019-04-29 18:11

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0011_auto_20190429_2006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialuser',
            name='site_internet',
            field=models.TextField(default='https://davidpavlovschii.netlify.com', null=True, validators=[django.core.validators.URLValidator()]),
        ),
    ]
