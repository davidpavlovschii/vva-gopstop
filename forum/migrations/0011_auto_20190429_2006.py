# Generated by Django 2.0.10 on 2019-04-29 18:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0010_auto_20190429_2005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialuser',
            name='bio',
            field=models.TextField(default='Je suis un gopnik en herbe.', max_length=4096, null=True),
        ),
        migrations.AlterField(
            model_name='specialuser',
            name='fun_fact',
            field=models.TextField(default="J'ai grimpé le mont everest sur les mains.", max_length=4096),
        ),
    ]
