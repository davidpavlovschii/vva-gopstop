from django.contrib import admin
from forum.models import SpecialUser, Post, Category, Comment, UserProfilePic
# Register your models here.
admin.site.register(Post)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(SpecialUser)
admin.site.register(UserProfilePic)