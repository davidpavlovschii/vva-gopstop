from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from forum.models import Post, SpecialUser, Category, Comment
from datetime import timedelta, datetime
from django.utils.text import slugify
from django.contrib import messages
import json
from django.contrib.auth import authenticate


def post_like(request, id):
    try:
        post = get_object_or_404(Post, id=id)
        if request.user.is_anonymous:
            messages.error(request, 'Vous devez vous connecter pour pouvoir liker un post')
            return redirect('forum:post-detail', id = id)
        #Si l'utilisateur n'as pas déjà liké le post et n'as pas disliké le post
        elif request.user not in post.likes.all() and request.user not in post.dislikes.all():
            #Ajout d'un simple like au post
            post.likes.add(request.user)
            messages.success(request, 'Vous avez bien liké ce post')
            return redirect('forum:post-detail', id = id)
        #Si l'utilisateur n'as pas déjà liké le post et par contre a déjà disliké le post
        elif request.user not in post.likes.all() and request.user in post.dislikes.all():
            #Le dislike de l'utilisateur sur le post est supprimé
            post.dislikes.remove(request.user)
            #Un like est ajouté
            post.likes.add(request.user)
            messages.info(request, "Votre dislike à bien été changé en like")
            return redirect('forum:post-detail', id = id)
        else:
            messages.info(request, 'Votre like à bien été retiré')
            post.likes.remove(request.user)
            return redirect('forum:post-detail', id = id)
    except Exception as e:
        raise e


def post_dislike(request, id):
    try:
        post = get_object_or_404(Post, id=id)
        if request.user.is_anonymous:
            messages.error(request, 'Vous devez vous connecter pour pouvoir disliker un post')
            return redirect('forum:post-detail', id = id)
        elif request.user not in post.dislikes.all() and request.user not in post.likes.all():
            post.dislikes.add(request.user)
            messages.success(request, 'Vous avez bien disliké ce post')
            return redirect('forum:post-detail', id = id)
        elif request.user not in post.dislikes.all() and request.user in post.likes.all():
            post.likes.remove(request.user)
            post.dislikes.add(request.user)
            messages.info(request, "Votre like à bien été changé en dislike")
            return redirect('forum:post-detail', id = id)
        else:
            messages.info(request, 'Votre dislike à bien été retiré')
            post.dislikes.remove(request.user)
            return redirect('forum:post-detail', id = id)
    except Exception as e:
        raise e