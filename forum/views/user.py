from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.utils import timezone
from forum.models import Post, SpecialUser, Category, Comment, UserProfilePic
from datetime import timedelta, datetime
from django.contrib import messages
from django.utils.text import slugify
from forum.utils.user import get_cleaned_posts
from forum.utils.time import datetime_humanize
import json
from django.contrib.auth import authenticate, logout


def user_posts(request):
    user_is_connected = True if request.user.is_authenticated else False
    posts = Post.objects.filter(user = request.user)
    post_content = get_cleaned_posts(posts)
    context = {
        'user_is_connected' : user_is_connected,
        'post_content': post_content,   
    }
    return render(request, 'forum/user_posts.html', context= context)

def user_history(request):
    user_is_connected = True if request.user.is_authenticated else False
    posts = Post.objects.all()
    liked_posts = []
    for post in posts: 
        if request.user in post.likes.all():
            liked_posts.append(post)
    post_content = get_cleaned_posts(liked_posts)
    context = {
        'user_is_connected' : user_is_connected,
        'post_content': post_content,   
    }
    return render(request, 'forum/user_history.html', context= context)

def user_profile(request):
    user_is_connected = True if request.user.is_authenticated else False
    user_posts = Post.objects.filter(user = request.user)
    post_content = get_cleaned_posts(user_posts)
    has_model_pic = True if request.user.specialuser.user_profile_pic else False
    user_profile_pic_obj = UserProfilePic.objects.get(specialuser = request.user.specialuser) if has_model_pic else None
    user_profile_pic = user_profile_pic_obj.model_pic if has_model_pic else None
    all_posts = Post.objects.all()
    liked_posts = []
    for post in all_posts: 
        if request.user in post.likes.all():
            liked_posts.append(post)
    date_joined = datetime_humanize(request.user.date_joined)
    last_login = datetime_humanize(request.user.last_login)
    context = {
        'user_is_connected' : user_is_connected,
        'post_content': post_content,
        'like_number' : len(liked_posts),
        'post_number' : user_posts.count(),
        'date_joined' : date_joined,
        'last_login' : last_login,   
        'has_model_pic' : has_model_pic,
        'user_profile_pic' : user_profile_pic,
    }
    return render(request, 'forum/user_profile.html', context= context)



def user_add_profile_picture(request):
    user_is_connected = True if request.user.is_authenticated else False
    if request.POST and user_is_connected:
        specialuser = SpecialUser.objects.filter(user = request.user)
        user_profile_pic = UserProfilePic.objects.create(model_pic = request.FILES["model_pic"])
        specialuser.update(user_profile_pic = user_profile_pic)
    return redirect('forum:user-profile')

def user_update_information(request):
    user_is_connected = True if request.user.is_authenticated else False
    if request.POST and user_is_connected:
        user = User.objects.filter(id = request.user.id)
        specialuser = SpecialUser.objects.filter(user = request.user)
        user.update(
                username = request.POST["username"],
                first_name = request.POST["first_name"],
                last_name = request.POST["last_name"], 
                email = request.POST["email"], 
        )
        specialuser.update(
            bio = request.POST["bio"],
            fun_fact = request.POST["fun_fact"],
        )
        messages.success(request, 'Le post à bien été modifié')

    return redirect('forum:user-profile')

def user_update_password(request):
    user_is_connected = True if request.user.is_authenticated else False
    if request.POST and user_is_connected:
        user = User.objects.get(id = request.user.id)
        if request.POST["password"] == request.POST["conf_password"]:
            user.set_password(request.POST["password"])
            messages.success(request, 'Le mot de passe est bien modifié')
        else:
            messages.error(request, 'Vos mots de passes ne correspondent pas.')
    return redirect('forum:user-profile')

def user_profile_delete(request):
    user_is_connected = True if request.user.is_authenticated else False
    user = User.objects.filter(id = request.user.id)
    if user_is_connected:
        logout(request)
        user.delete()
        messages.success(request, "Votre compte à bien été supprimé, il vous sera maintenant impossible de recréer un compte")
    else:
        messages.warning(request, "Petit margoulin arrête tes conneries.")
    return redirect('forum:home')
