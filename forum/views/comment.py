from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from forum.models import Post, SpecialUser, Category, Comment
from datetime import timedelta, datetime
from django.utils.text import slugify
from django.contrib import messages
import json
from django.contrib.auth import authenticate

def new_comment(request, post_id):
    if request.user.is_authenticated:
        if request.POST:
            comment = Comment.objects.create(
                post = get_object_or_404(Post, id = post_id), 
                description = request.POST["description"], 
                user = request.user,
                )
            comment.save()
    return redirect('forum:post-detail', id = post_id)

def comment_delete(request, post_id, comment_id):
    try:
        user_comment = get_object_or_404(Comment, id = comment_id)
        #Si l'utilisateur connecté est l'utilisateur qui à créer le commentare ou il fait parti du staff
        if request.user.is_authenticated and request.user == user_comment.user or request.user.is_staff: 
            #alors il peut supprimer le commentaire
            Comment.objects.filter(id=comment_id).delete()
            messages.success(request, 'Le commentaire à bien été supprimé')
        else:
            messages.warning(request, "Vous ne pouvez pas supprimer ce commentaire car vous n'êtes pas l'auteur")            
        return redirect('forum:post-detail', id = post_id)
    except Exception as e:
        raise e

def comment_update(request, post_id, comment_id):
    try:
        user_comment = get_object_or_404(Comment, id = comment_id)
        if request.user.is_authenticated and request.user == user_comment.user:
            Comment.objects.filter(id = comment_id).update(description = request.POST["description"])
            messages.success(request, 'Le commentaire à bien été modifié')
            return redirect('forum:post-detail', id = post_id)
    except Exception as e:
        raise e