from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from forum.models import Post, SpecialUser, Category, Comment
from forum.utils.user import get_cleaned_posts
from datetime import timedelta, datetime
from django.contrib import messages
from django.utils.text import slugify
from django.contrib.auth import authenticate
import json

def new_post(request):
    user_is_connected = True if request.user.is_authenticated else False
    if request.user.is_authenticated:
        if request.POST:
            post = Post.objects.create(
                user = request.user,
                description = request.POST["description"], 
                title = request.POST["title"], 
                model_pic = request.FILES["model_pic"],
                category = Category.objects.get(id = request.POST['category'])
                )
            post.save()
            return redirect('forum:post-detail', id = post.id)
        else:
            categories = Category.objects.all()
            user_is_connected = True
            context = {
                'user_is_connected' : user_is_connected,
                'categories' : categories
            }
            return render(request, 'forum/new_post.html', context= context) 
    return redirect('forum:home')

def post_detail(request, id):
    try: 
        user_is_connected = True if request.user.is_authenticated else False
        post = get_object_or_404(Post, id = id)
        like_number = post.likes.count()
        dislike_number = post.dislikes.count()
        post_user = post.user
        post_user_profile_pic = None
        if post_user.specialuser.user_profile_pic is not None:
            post_user_profile_pic = post_user.specialuser.user_profile_pic.model_pic
        comments = Comment.objects.filter(post = post)
        context = {
            'post': post,
            'comments' : comments,
            'user_is_connected' : user_is_connected,
            'like_number' : like_number,
            'dislike_number': dislike_number,
            'post_user_profile_pic' : post_user_profile_pic,
        }
        return render(request, 'forum/post_detail.html', context= context) 
    except Exception as e: 
        raise e

def post_delete(request, id):
    try:
        post = get_object_or_404(Post, id=id)
        if request.user.is_authenticated and post.user == request.user or request.user.is_staff:
            messages.success(request, 'Le post intitulé "' + post.title + '" à bien été supprimé')
            post.delete()
        else:
            messages.warning(request, "vous ne pouvez pas supprimer le poste intitulé '" + post.title + "' car vous n'êtes pas l'auteur") 
        return redirect('forum:home')
    except Exception as e:
        raise e

def post_update(request, id):
    user_is_connected = True if request.user.is_authenticated else False
    #get_object_or_404 retourn un objet, donc les propriétés sont accessible
    user_post = get_object_or_404(Post, id=id)
    if request.user.is_authenticated and user_post.user == request.user or request.user.is_staff:
        #posts.objects.filter() retourne une collection d'objet, les propriétés ne sont pas accessibles
        post = Post.objects.filter(id=id)
        if request.POST:
            post.update(
                user = request.user,
                description = request.POST["description"], 
                title = request.POST["title"], 
                category = Category.objects.get(id = request.POST['category'])
                )
            messages.success(request, 'Le post à bien été modifié')
            return redirect('forum:post-detail', id = id)
        else:
            categories = Category.objects.all()
            post = Post.objects.get(id = id)
            context = {
                'user_is_connected' : user_is_connected,
                'post' : post,
                'categories' : categories,
            }
            return render(request, 'forum/post_update.html', context= context) 
    else:
        messages.warning(request, "vous ne pouvez pas modifier le poste intitulé '" + post.title + "' car vous n'êtes pas l'auteur") 
    return redirect('forum:home')

def post_search(request):
    user_is_connected = True if request.user.is_authenticated else False
    if request.POST:
        posts = Post.objects.filter(title__icontains= request.POST["recherche"])
        post_content = get_cleaned_posts(posts)
        context = {
            'user_is_connected' : user_is_connected,
            'post_content': post_content,   
        }
        if posts.count() == 0:
            messages.warning(request, "Aucun résultat pour la recherche '" + request.POST["recherche"] + "'.")
            return redirect('forum:home')
        else:
            messages.info(request, "Résultat(s) pour la recherche '" + request.POST["recherche"] +  "'.")
            return render(request, 'forum/search_result.html', context = context)
    return redirect('forum:home')