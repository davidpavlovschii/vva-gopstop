from django.shortcuts import render, redirect
from django.utils import timezone
from forum.models import *
from forum.utils.account import is_connected
from django.contrib.auth import forms, authenticate as django_auth, login as django_log, logout as django_logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.urls import reverse
from forum.forms import UserRegisterForm
import urllib
import hashlib
import datetime



def register(request):
    """
    Handle the connection of the login page.
    """
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Compte créer pour ' + username + '!')
            return redirect('forum:home')
    else:
        form = UserRegisterForm()
    return render(request, "forum/register.html", {'form':form} )

def login(request):
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = django_auth(request, username=username, password=password)
        if user is not None:
            django_log(request, user)
            return redirect('forum:home')
        else:
            messages.warning(request, "Nom d'utilisateur ou mot de passe incorrecte.")
            return redirect('forum:login')
    return render(request, 'forum/login.html')

@login_required
def logout(request):
    django_logout(request)
    return redirect('forum:home')

def reset_password(request):
    return render(request, 'forum/reset_password.html')
