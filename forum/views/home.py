from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from datetime import timedelta, datetime
from forum.models import Post, SpecialUser, Category, Comment
from forum.utils.user import get_cleaned_posts
from django.utils.text import slugify
import json
from django.contrib.auth import authenticate


def home(request, cat_id = None):
    user_is_connected = False
    #Recupére l'objet Category a partir de son id passé en parametre s'il est défini, sinon la valeur est nulle
    categ = get_object_or_404(Category, id= cat_id) if cat_id is not None else None
    #Récupère la liste des postes qui ont pour catégorie "categ" si le parametre cat_id est specifié, sinon on récupère tous les posts
    posts = Post.objects.all() if cat_id is None else Post.objects.filter(category = categ)

    if request.user.is_authenticated:
        user_is_connected = True
    post_content = get_cleaned_posts(posts)


    
    cat_content = []
    for category in Category.objects.all():
        cat_content.append({
            'category' : category,
            'count' : Post.objects.filter(category = category).count(),
        })

    context = {
        'user_is_connected' : user_is_connected,
        'post_content': post_content,   
        'cat_content' : cat_content,
        #Retourne la catégorie passée en parametre dans le request pour pouvoir afficher son nom dans le message du template "Il n'y a aucun post pour la catégorie pour le moment"
        'categ' : categ,
    }
    return render(request, 'forum/home.html', context= context)




