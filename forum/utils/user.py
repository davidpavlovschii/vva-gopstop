from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from forum.models import Post, SpecialUser, Category, Comment
from datetime import timedelta, datetime
from forum.utils.time import datetime_humanize
from django.contrib import messages
from django.utils.text import slugify
import json
from django.contrib.auth import authenticate


def get_cleaned_posts(posts):
    post_content = []

    for post in posts:
        post_creat_time = datetime_humanize(post.dt_creation)
        post_content.append({
            'post' : post,
            'count' : Comment.objects.filter(post = post).count(),
            'date': post_creat_time,
        })
    return post_content