def find_second_from_first_in_tuples(tuples, first):
    """
        Find a tuple from a list thanks to the first tuple elem.
        By default return the second element or an empty string if not found.
        Parameters:
            tuples<(string, string)>: The tuple list.
            first<string>: The first element of a tuple.
    """
    for e in tuples:
        if e[0] == first:
            return e[1]
    # Tuple not found
    return ""