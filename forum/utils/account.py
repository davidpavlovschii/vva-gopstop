import datetime

def is_connected(request, category="", minimal_category=""):
    """
            Check if the user is connected,
            if requested it as the specifque category
            Parameters:
                    request<request>: The request which contain the url.path
                    *category<string>: The specific user category
                    *minimal_category<string>: The specific user category
    """
    from forum.models import User
    from GopStop.settings import TOKEN_MAX_VALIDITY as token_max_validity
    # Check if there is token in session
    if 'token' not in request.session:
        return False
    # Get token in session
    token = request.session['token']
    try:
        # Get user according the token
        user = None
        if len(category) == 0:
            user = User.objects.get(token=token)
            # Check if a minimal category is requiered
            if len(minimal_category) != 0:
                # Init vars
                checking = False
                find = False
                # Loop on each Categories for the end to start
                for c in reversed(User.CATEGORY_CHOICES):
                    # Try to find the minimal_category into choice list
                    if c[1] == minimal_category:
                        checking = True
                    if checking and c[0] == user.category:
                        find = True
                        break
                if not find:
                    return False
        else:
            choices = User.CATEGORY_CHOICES
            category_val = 0
            for c in choices:
                if c[1] == category:
                    category_val = c[0]
                    break
            user = User.objects.get(token=token, category=category_val)
    except Exception:
        return False
    # User never connected
    if user.connected is None:
        return False
    now = datetime.datetime.now().replace(tzinfo=None)
    if (now - user.connected.replace(tzinfo=None)) > datetime.timedelta(
            days=token_max_validity):
        # Token timeout, cleaning
        user.token = ""
        # User update
        user.save()
        return False
    # Everything is ok
    return True

def send_access_by_email(user):
    """
    Send all the user access the to user by email.
    
    :param app.User user: The user model
    """
    from GopStop.settings import EMAIL_HOST_USER
    from forum.utils.external import send_mail
    if user.password and len(user.password):
        context = {
            "username": user.username,
            "password": user.password
        }
        send_mail("Accés GopStop", [user.email], EMAIL_HOST_USER, context, 'forum/email/client_access.html')
    else:
        print("Register").warning("Cannot send access to the user '{}', the info field seems empty".format(user.username))
