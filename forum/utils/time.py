import calendar
import datetime
from django.utils import timezone

def time_format(t):
    """
            Format a datetime and return it
            Parameters:
                    t<time>: The datetime to format
    """
    return t.strftime("%Y-%m-%d %H:%M:%S")

def datetime_humanize(dt):
    """
        Convert a datetime into a pretty and
        Get a datetime object or a int() Epoch timestamp and return a
        pretty string like 'an hour ago', 'Yesterday', '3 months ago',
        'just now', etc
    """
    now = timezone.now().replace(tzinfo=None)
    diff = now - dt.replace(tzinfo=None)
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return 'Future date'

    if day_diff == 0:
        if second_diff < 10:
            return "A l'instant"
        elif second_diff < 60:
            return  str(second_diff) + " secondes"
        elif second_diff < 120:
            return "une minute"
        elif second_diff < 3600:
            return  str(round(second_diff / 60, 1)) + " minutes"
        elif second_diff < 7200:
            return "une heure"
        elif second_diff < 86400:
            return str(round(second_diff / 3600, 1)) + " heures"
    elif day_diff == 1:
        return "Hier"
    elif day_diff < 7:
        return str(day_diff) + " jours"
    elif day_diff < 31:
        return str(round(day_diff / 7, 1)) + " semaines"
    elif day_diff < 365:
        return str(round(day_diff / 30, 1)) + " mois"
    return str(round(day_diff / 365, 1)) + " années"