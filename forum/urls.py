from django.conf.urls import url
from django.urls import path
from django.conf import settings
from forum.views import home as home, post, account, comment, vote, user

app_name = "forum"
urlpatterns = [
    path('', home.home, name='default'),
    path('home', home.home, name='home'),
    path('home/cat_sort/<int:cat_id>', home.home, name='cat-sort'),
    path('login', account.login, name='login'),
    path('logout', account.logout, name='logout'),
    path('register', account.register, name='register'),
    path('reset_password', account.reset_password, name='reset-password'),
    path('new_post', post.new_post, name='new-post'),
    path('post/detail/<int:id>', post.post_detail, name='post-detail'),
    path('post/detail/<int:post_id>/new_comment', comment.new_comment, name='new-comment'),
    path('post/detail/<int:post_id>/comment_delete/<int:comment_id>', comment.comment_delete, name='comment-delete'),
    path('post/detail/<int:post_id>/comment_update/<int:comment_id>', comment.comment_update, name='comment-update'),
    path('post/delete/<int:id>', post.post_delete, name='post-delete'),
    path('post/update/<int:id>', post.post_update, name='post-update'),
    path('search', post.post_search, name='post-search'),
    path('post/detail/<int:id>/like', vote.post_like, name='post-like'),
    path('post/detail/<int:id>/dislike', vote.post_dislike, name='post-dislike'),
    path('user/posts', user.user_posts, name='user-posts'),
    path('user/history', user.user_history, name='user-history'),
    path('user/profile', user.user_profile, name='user-profile'),
    path('user/profile/update/information', user.user_update_information, name='user-update-information'),
    path('user/profile/update/password', user.user_update_password, name='user-update-password'),
    path('user/profile/delete', user.user_profile_delete, name='user-profile-delete'),
    path('user/profile/add_profile_picture', user.user_add_profile_picture, name='user-add-profile-picture'),
]
