
from forum.utils.tuple import find_second_from_first_in_tuples
from django.core.exceptions import ObjectDoesNotExist
from forum.utils.time import time_format, datetime_humanize
from django.contrib.auth.models import User as DjangoUser
from django.core.files.storage import FileSystemStorage
from django.db.models.signals import pre_delete, post_save
from django.dispatch.dispatcher import receiver
from forum.utils.string import encode_str_from_key
from django.core.validators import URLValidator
from django.conf import settings
from django.db import models
import datetime
import os
import json

fs = FileSystemStorage(location="/media/")

class UserProfilePic(models.Model):
    model_pic = models.ImageField(null=True)
    def __str__(self):
        return "{0}".format(self.model_pic)
class SpecialUser(models.Model):
    user_profile_pic = models.ForeignKey(UserProfilePic, null=True, on_delete=models.SET_NULL, blank=True)
    user = models.OneToOneField(DjangoUser,null=True, on_delete=models.CASCADE)
    bio = models.TextField(max_length=4096, null=True, default="Je suis un gopnik en herbe.")
    fun_fact = models.TextField(max_length=4096, default="J'ai grimpé le mont everest sur les mains.")
    is_romesh = models.BooleanField(default=False)
    site_internet = models.TextField(validators=[URLValidator()], null=True, default="https://davidpavlovschii.netlify.com")

    def create_specialuser(sender, **kwargs):
        if kwargs['created']:
            SpecialUser.objects.create(user = kwargs['instance'])
    post_save.connect(create_specialuser, sender=DjangoUser)

    def delete(self, *args, **kwargs):
        if os.path.isfile(self.model_pic.path):
            os.remove(self.model_pic.path)
        super(SpecialUser, self).delete(*args,**kwargs)

    def __str__(self):
        return self.user.username
  
class Category(models.Model):
    title = models.CharField(max_length=256)
    model_pic = models.ImageField(
        upload_to= 'media' , default= 'media/avatar.jpg')
    description = models.CharField(max_length=5000)
    def __str__(self):
        return self.title
    class Meta:
        ordering = ('title',)


class Post(models.Model):
    user = models.ForeignKey(DjangoUser, on_delete=models.CASCADE, default=1, related_name="user_name")
    likes = models.ManyToManyField(DjangoUser, blank=True, related_name="likes")
    dislikes = models.ManyToManyField(DjangoUser, blank=True, related_name="dislikes")
    title = models.CharField(max_length=256)
    model_pic = models.ImageField(default="eh.PNG")
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.CharField(max_length=5000)
    dt_creation = models.DateTimeField(blank=True, auto_now_add=True)
    def delete(self, *args, **kwargs):
        if os.path.isfile(self.model_pic.path):
            os.remove(self.model_pic.path)
        super(Post, self).delete(*args,**kwargs)

    def __str__(self):
        return self.title

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, related_name='comments')
    user = models.ForeignKey(DjangoUser, on_delete=models.CASCADE, null=True, related_name='user')
    description = models.CharField(max_length=5000)
    dt_creation = models.DateTimeField(blank=True, auto_now_add=True)

    def __str__(self):
        return self.description 